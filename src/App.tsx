import { useState } from 'react'
import useSWR from 'swr';
import moment from 'moment';
import { fetcher } from './utils'

const App = () => {
  const [page, setPage] = useState<number>(1)
  const { data, error } = useSWR<any[]>(
    `repos?per_page=15&page=${page}`,
    fetcher, { refreshInterval: 10000 }
  );

  return (
    <div className="container mx-auto my-5 ">
      {
        error ? (<>

          <div className="flex justify-center mt-5 mx-8">
            <section className="w-10/12  sm:w-10/11 lg:w-1/2 max-w-2xl flex justify-center">
              <h1 className="text-9xl text-bolder text-red-700"> ERROR </h1>
            </section>
          </div>

        </>) : (<>
          {
            !data ? (
              <>
                <div className="flex justify-center mt-5 mx-8">
                  <section className="w-10/12  sm:w-10/11 lg:w-1/2 max-w-2xl flex justify-center">
                    <h1 className="text-8xl text-bolder text-yellow-700"> Loading ...... </h1>
                  </section>
                </div>
              </>
            ) : (
              <>
                <div className="grid grid-flow-row auto-rows-max lg:mt-5 sm:mt-2">
                  {
                    data.map((repo: any, i: number) => {
                      return (
                        <div className="flex justify-center mt-5 mx-8" key={i}>
                          <section className="w-10/12  sm:w-10/11 lg:w-1/2 max-w-2xl">
                            <h2 className="font-bold ml-2">{repo.name}, <span className="font-light text-xs">{moment(repo.created_at).fromNow()}</span></h2>
                            <p className="text-left text-sm px-1 text-gray-700">
                              Language: {repo.language}
                            </p>
                          </section>
                        </div>
                      )
                    })
                  }
                </div>
                <div className="flex justify-center mt-5 mx-8">
                  <section className="w-10/12  sm:w-10/11 lg:w-1/2 max-w-2xl flex justify-center">
                    <nav aria-label="Page navigation">
                      <ul className="inline-flex space-x-2">
                        {
                          [...Array(10)].map((val, i) =>
                            <li key={i}>
                              <button className={`w-10 h-10 ${i + 1 === page ? " text-white transition-colors duration-150 bg-gray-500 border border-r-0 border-gray-500 rounded-full focus:shadow-outline" : "transition-colors duration-150 rounded-full focus:shadow-outline hover:bg-gray-100"} `} onClick={() => setPage(i + 1)}>{i + 1}</button>
                            </li>
                          )
                        }
                      </ul>
                    </nav>
                  </section>
                </div>
              </>
            )
          }</>)
      }
    </div >
  );
}
export default App;