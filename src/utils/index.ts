import axios from "axios";

export const fetcher = async (url: string) => {
    const api = axios.create({
        baseURL: 'https://api.github.com/users/janakhpon/',
    });
    let data = await api.get(url);
    return data.data;
}
